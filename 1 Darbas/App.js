import React from 'react';
import { StyleSheet, Text, View, Button, TouchableOpacity, Alert, state, props, TouchableHighlight  } from 'react-native';

export default class App extends React.Component {
  constructor(props) {
    super(props)
  this.state = {
    color1: 'red',
    color2: 'red',
    color3: 'red',
    color4: 'red'
  }
}
  onPress1 = () => {
    if(this.state.color1 == 'red'){
      this.setState({
        color1: 'white'
      })
    }
    else{
      this.setState({
        color1: "red"
      })
    }
  }

  onPress2 = () => {
    if(this.state.color2 == 'red'){
      this.setState({
        color2: 'white'
      })
    }
    else{
      this.setState({
        color2: "red"
      })
    }
  }
  onPress3 = () => {
    if(this.state.color3 == 'red'){
      this.setState({
        color3: 'white'
      })
    }
    else{
      this.setState({
        color3: "red"
      })
    }
  }
  onPress4 = () => {
    if(this.state.color4 == 'red'){
      this.setState({
        color4: 'white'
      })
    }
    else{
      this.setState({
        color4: "red"
      })
    }
  }
render() {
  return (

<View style={{ flexDirection: 'row', flex: 1, justifyContent: 'center', alignItems: 'stretch', backgroundColor: '#61210B'}}>
<View style={{flex:1, justifyContent: 'space-around', backgroundColor:"green"}}>
<Button
 onPress={this.onPress1}
title="Pirmas"
color="#2E2EFE"
accessibilityLabel="1"
/>
<Button
 onPress={this.onPress2}
title="Antras"
color="#2E2EFE"
accessibilityLabel="2"
/>
<Button
 onPress={this.onPress3}
title="Trecias"
color="#2E2EFE"
accessibilityLabel="3"
/>
<Button
 onPress={this.onPress4}
title="Ketvirtas"
color="#2E2EFE"
accessibilityLabel="4"
/>
</View>
 <View style={{flex:1, justifyContent: 'space-around', alignItems:'center', backgroundColor:"white"}}>
      <View style={styles.deze} backgroundColor={this.state.color1} />
      <View style={styles.deze} backgroundColor={this.state.color2} />
      <View style={styles.deze} backgroundColor={this.state.color3} />
      <View style={styles.deze} backgroundColor = {this.state.color4}  />
</View>
</View>
  );
}
}

const styles = StyleSheet.create({
container: {
  flex: 1,
  backgroundColor: '#fff',
  flexDirection: 'row',

  justifyContent: 'center',
  alignItems: "flex-start"
},
deze: {
  width: 70,
  height: 75,
  backgroundColor: 'yellow'
},
button: {
  alignItems: 'center',
  backgroundColor: '#61210B',
  padding: 10
},
countContainer: {
  alignItems: 'center',
  padding: 10
},
countText: {
  color: '#FF00FF'
}
});
