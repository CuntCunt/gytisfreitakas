import React, { Component } from 'react';
import { View, Text, Button, Image } from 'react-native';

export class Pradinis extends Component {
  render() {
    return(
      <View>
        <Image
          style={{
            width: 170,
            height: 300

          }}
          source={require('./ft.png')} />
        <Text>Vardas: Gytis;</Text>
        <Text>Pavarde: Freitakas;</Text>
        <Button onPress={() => this.props.navigation.navigate('DarbasScreen')}
        title="Darbas"/>
        <Text> </Text>
        <Button onPress={() => this.props.navigation.navigate('AsmensGebejimaiScreen')}
        title="Asmeniniai sugebejimai"/>
      </View>
    )
  }
}
export default Pradinis;
